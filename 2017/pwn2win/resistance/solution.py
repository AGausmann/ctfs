#!/usr/bin/python3

import networkx as nx
import numpy as np
from pwn import *
import sys


def parse_input(data):
    data = data.strip().split('\n')
    edges = []
    queries = []
    for line in data:
        line = [int(word) for word in line.split(' ')]
        if len(line) == 3:
            edges.append(tuple(line))
        elif len(line) == 2:
            queries.append(tuple(line))

    return (edges, queries)

def build_graph(edges):
    G = nx.MultiGraph()
    for (a, b, resistance) in edges:
        G.add_edge(a, b, resistance=resistance)

    return G

def calc_res(G, queries):
    answers = []

    # Generate coefficient matrix
    coef = []
    for a in G.nodes():
        eq = [0] * (max(G.nodes()))
        for (u, v, resistance) in G.edges(a, data='resistance'):
            eq[u-1] += 1/resistance
            eq[v-1] -= 1/resistance

        coef.append(eq)
    coef = np.array(coef)

    for (src, dst) in queries:
        # Generate solution vector for this query
        sol = []
        for a in G.nodes():
            if a == src:
                sol.append(1)
            elif a == dst:
                sol.append(-1)
            else:
                sol.append(0)
        sol = np.array(sol)

        # Solve system
        result = np.linalg.lstsq(coef, sol)[0]
        answers.append(abs(result[src - 1] - result[dst - 1]))

    return answers

def main():
    # context.log_level = 'debug'
    rc = remote('programming.pwn2win.party', 9001, ssl=True)

    for i in range(10):
        (edges, queries) = parse_input(rc.recvuntil(['\n\n']).decode())
        print('Problem', i)

        G = build_graph(edges)
        answers = calc_res(G, queries)

        response = ''.join(['{:.3f}\n'.format(ans) for ans in answers])
        rc.send(response)

    # Flag
    print(rc.recvall().decode())

if __name__ == '__main__':
    main()
