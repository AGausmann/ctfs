# CSAW '17
## best_router (forensics 200)

https://ctf.csaw.io/challenges#Best%20Router

> http://forensics.chal.csaw.io:3287

> NOTE: This will expand to ~16GB!

### Recon

We are provided with a TAR archive `best_router.tar.gz` and a link to a website. The website is a simple interface that asks for a username and password. Our goal is probably to locate the username and password inside the TAR archive.

When we extract the TAR archive, it expands to a 16GB `best_router.img` file, which is probably a copy of a 16GB disk. Let's run Craig Heffner's `binwalk` to analyze the contents:
```
DECIMAL       HEXADECIMAL     DESCRIPTION
--------------------------------------------------------------------------------
4886543       0x4A900F        Copyright string: "copyright does *not* cover user programs that use kernel"
4886778       0x4A90FA        Copyright string: "copyrighted by the Free Software"
4886890       0x4A916A        Copyright string: "copyrighted by me and others who actually wrote it."
4887275       0x4A92EB        Copyright string: "Copyright (C) 1989, 1991 Free Software Foundation, Inc."
4889125       0x4A9A25        Copyright string: "copyright the software, and"
4890268       0x4A9E9C        Copyright string: "copyright holder saying it may be distributed"
4890509       0x4A9F8D        Copyright string: "copyright law:"
4891383       0x4AA2F7        Copyright string: "copyright notice and disclaimer of warranty; keep intact all the"
4892649       0x4AA7E9        Copyright string: "copyright notice and a"
4899523       0x4AC2C3        Copyright string: "copyrighted interfaces, the"
4899560       0x4AC2E8        Copyright string: "copyright holder who places the Program under this License"
4900778       0x4AC7AA        Copyright string: "copyrighted by the Free"
4902841       0x4ACFB9        Copyright string: "copyright" line and a pointer to where the full notice is found."
4902987       0x4AD04B        Copyright string: "Copyright (C) <year>  <name of author>"
4903963       0x4AD41B        Copyright string: "Copyright (C) year name of author"
4904579       0x4AD683        Copyright string: "copyright disclaimer" for the program, if"
4904708       0x4AD704        Copyright string: "copyright interest in the program"
4905472       0x4ADA00        Copyright string: "Copyright (c) 2006, Broadcom Corporation."
4905514       0x4ADA2A        Copyright string: "Copyright (c) 2015, Raspberry Pi (Trading) Ltd"
4905879       0x4ADB97        Copyright string: "copyright notice and the"
5072524       0x4D668C        Unix path: /usr/lib/raspi-config/init_resize.sh
5105700       0x4DE824        Linux kernel ARM boot executable zImage (little-endian), load address: "0x00000000", end address: "0x0042DA20"
5123608       0x4E2E18        gzip compressed data, maximum compression, from Unix, NULL date (1970-01-01 00:00:00)
9487908       0x90C624        Linux kernel ARM boot executable zImage (little-endian), load address: "0x00000000", end address: "0x0045E6C8"
9506144       0x910D60        gzip compressed data, maximum compression, from Unix, NULL date (1970-01-01 00:00:00)
14069248      0xD6AE00        ELF, 32-bit LSB executable, version 1 (SYSV)
14401922      0xDBC182        Unix path: /../../../../codecs/video/hw/dec3/h264/h264_requirements.c
14402258      0xDBC2D2        Unix path: /../../../../codecs/video/hw/dec3/mpeg2/mpeg2_requirements.c
14402650      0xDBC45A        Unix path: /../../../../codecs/video/hw/dec3/vc1/vc1_requirements.c
14421306      0xDC0D3A        Unix path: /../../../../vcfw/startup/startup.c
14472782      0xDCD64E        Unix path: /../../../../applications/vmcs/vmcs.c
14547910      0xDDFBC6        Unix path: /../../../../helpers/arm_loader/arm_display.c
14564446      0xDE3C5E        Unix path: /../../../../helpers/arm_loader/arm_loader.c
14565674      0xDE412A        Unix path: /../../../../vcfw/drivers/chip/vciv/2708/arm.c
14580026      0xDE793A        Unix path: /../../../../applications/vmcs/audioserv/audio_server.c
14601530      0xDECD3A        Unix path: /../../../../filesystem/media/brfs/brfs.c
```

Whew, that was a lot of lines. I've truncated the output listed above since most of it isn't needed. Here's some interesting information: This seems to be an image of a Raspberry Pi SD card, since we can see several Pi-specific files as well as some Linux image files. Let's load this image as a disk, mount the filesystems and see what's going on:
```
# partx -a ./best_router.img /dev/loop0partition: none, disk: ./best_router.img, lower: 0, upper: 0
Trying to use '/dev/loop1' for the loop device
/dev/loop1: partition table type 'dos' detected
range recount: max partno=2, lower=0, upper=0
/dev/loop1: partition #1 added
/dev/loop1: partition #2 added
# mkdir /tmp/sdp1 /tmp/sdp2
# mount /dev/loop1p1 /tmp/sdp1
# mount /dev/loop1p2 /tmp/sdp2
```

Okay, now we are able to browse the filesystems:
```
$ ls -l /tmp/sdp1 /tmp/sdp2
/tmp/sdp1:
total 21197
-rwxr-xr-x 1 root root   15660 May 15 14:09 bcm2708-rpi-0-w.dtb
-rwxr-xr-x 1 root root   15197 May 15 14:09 bcm2708-rpi-b.dtb
-rwxr-xr-x 1 root root   15456 May 15 14:09 bcm2708-rpi-b-plus.dtb
-rwxr-xr-x 1 root root   14916 May 15 14:09 bcm2708-rpi-cm.dtb
-rwxr-xr-x 1 root root   16523 May 15 14:09 bcm2709-rpi-2-b.dtb
-rwxr-xr-x 1 root root   17624 May 15 14:09 bcm2710-rpi-3-b.dtb
-rwxr-xr-x 1 root root   16380 May 15 14:09 bcm2710-rpi-cm3.dtb
-rwxr-xr-x 1 root root   50248 Aug 11 12:03 bootcode.bin
-rwxr-xr-x 1 root root     142 Dec 31  1979 cmdline.txt
-rwxr-xr-x 1 root root    1590 Sep  7 10:05 config.txt
-rwxr-xr-x 1 root root   18693 Aug 21  2015 COPYING.linux
-rwxr-xr-x 1 root root    2594 Aug 11 12:03 fixup_cd.dat
-rwxr-xr-x 1 root root    6688 Aug 11 12:03 fixup.dat
-rwxr-xr-x 1 root root    9834 Aug 11 12:03 fixup_db.dat
-rwxr-xr-x 1 root root    9830 Aug 11 12:03 fixup_x.dat
-rwxr-xr-x 1 root root     145 Sep  7 10:54 issue.txt
-rwxr-xr-x 1 root root 4581064 Aug 11 12:03 kernel7.img
-rwxr-xr-x 1 root root 4381216 Aug 11 12:03 kernel.img
-rwxr-xr-x 1 root root    1494 Nov 18  2015 LICENCE.broadcom
-rwxr-xr-x 1 root root   18974 Sep  7 10:54 LICENSE.oracle
drwxr-xr-x 2 root root   10240 Sep  7 09:58 overlays
-rwxr-xr-x 1 root root  666404 Aug 11 12:03 start_cd.elf
-rwxr-xr-x 1 root root 5007204 Aug 11 12:03 start_db.elf
-rwxr-xr-x 1 root root 2868132 Aug 11 12:03 start.elf
-rwxr-xr-x 1 root root 3952100 Aug 11 12:03 start_x.elf

/tmp/sdp2:
total 88
drwxr-xr-x  2 root root  4096 Sep  7 10:04 bin
drwxr-xr-x  2 root root  4096 Sep  7 10:53 boot
drwxr-xr-x  4 root root  4096 Sep  7 09:47 dev
drwxr-xr-x 84 root root  4096 Sep  9 23:23 etc
drwxr-xr-x  3 root root  4096 Sep  7 09:59 home
drwxr-xr-x 16 root root  4096 Sep  7 10:13 lib
drwx------  2 root root 16384 Sep  7 10:53 lost+found
drwxr-xr-x  2 root root  4096 Sep  7 09:44 media
drwxr-xr-x  2 root root  4096 Sep  7 09:44 mnt
drwxr-xr-x  3 root root  4096 Sep  7 09:59 opt
drwxr-xr-x  2 root root  4096 Jul 27 17:17 proc
drwx------  3 root root  4096 Sep  9 23:52 root
drwxr-xr-x  5 root root  4096 Sep  7 10:00 run
drwxr-xr-x  2 root root  4096 Sep  7 10:13 sbin
drwxr-xr-x  2 root root  4096 Sep  7 09:44 srv
drwxr-xr-x  2 root root  4096 Jul 27 17:17 sys
drwxrwxrwt  7 root root  4096 Sep  9 23:52 tmp
drwxr-xr-x 10 root root  4096 Sep  7 09:44 usr
drwxr-xr-x 12 root root  4096 Sep  9 23:20 var
```

It looks like the first partition is the boot partition, and the second partition is the filesystem root, as we would have expected from a Raspberry Pi image. At this point I was pretty sure that the remote location was a web server running on a Raspberry Pi with this image, so let's find the webroot:

```
$ ls -l /tmp/sdp2/var/www/
total 16
-rw-r--r-- 1 root root    0 Sep  9 23:43 flag.txt
-rwxr-xr-x 1 root root  472 Sep  9 23:51 index.pl
-rwxr-xr-x 1 root root 1238 Sep  9 23:50 login.pl
-rw-r--r-- 1 adam adam   23 Sep  9 23:49 password.txt
-rw-r--r-- 1 adam adam    5 Sep  9 23:49 username.txt
```

There's a `flag.txt` file here! Unfortunately, they did have enough sense to clear it before imaging the filesystem. However, there are also `username.txt` and `password.txt`, which likely are the username and password needed for the remote server!

```
$ cat username.txt password.txt
admin
iforgotaboutthemathtest
```

Remember to clean up after you're done:
```
# umount /tmp/sdp1 /tmp/sdp2
# partx -d /dev/loop1
```

### Solution

Log into the remote server using the username and password we found. The server will send us the flag once we have authenticated.

Flag: `flag{but_I_f0rgot_my_my_math_test_and_pants}`
