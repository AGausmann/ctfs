# CSAW '17

[Main Website](https://ctf.csaw.io/) -
[Challenge Repository](https://github.com/isislab/CSAW-CTF-2017-Quals)

2017/09/15 20:00 UTC - 2017/09/17 20:00 UTC


## writeups

- [baby_crypt (crypto 350)](baby_crypt/)
- [best_router (forensics 200)](best_router/)
- [pilot (pwn 75)](pilot/)
- [scv (pwn 100)](scv/)
